using System.Collections.Generic;
using Lexer;

namespace Parser.AST
{
    public class BracketsNode : ArithmeticNode, INodesContainer
    {
        private bool _isComplete;
        private readonly ContainerNode _block = new ContainerNode();

        public IEnumerable<ASTNode> Children => _block.Children;

        public override bool IsComplete => _block.IsComplete && _isComplete;

        public override void Consume(Token token)
        {
            if (!_block.IsComplete)
            {
                _block.Consume(token);
                return;
            }

            switch (token.TokenType)
            {
                case TokenType.CloseBracket:
                    _isComplete = true;
                    return;
                default:
                    _block.Consume(token);
                    return;
            }
        }

        public override string ToString() 
        {
            var end = IsComplete ? ")" : string.Empty;
            return $"Bracket({_block}{end}";
        }
    }
}
