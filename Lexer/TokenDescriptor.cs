namespace Lexer
{
    class TokenDescriptor
    {
        private readonly SyntaxNode _syntax;
        public TokenType Type { get; }
        
        public string Keyword => _syntax.Keyword;
        public string Set => _syntax.Set;

        public TokenDescriptor(TokenType type, SyntaxNode syntax)
        {
            _syntax = syntax;
            Type = type;
        }
    }
}