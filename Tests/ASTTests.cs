using Xunit;
using FluentAssertions;
using Xunit.Abstractions;
using System.Linq;
using Lexer;
using Parser.AST;

namespace Tests
{
    public class ASTTests
    {
        private readonly ITestOutputHelper _output;

        public ASTTests(ITestOutputHelper output)
        {
            this._output = output;
        }

        [Fact]
        public void BasicMath()
        {
            // 1+1
            var root = new RootNode();
            root.Consume(Token.FromValue(TokenType.Digit, 1));
            root.Consume(Token.Plus);
            root.Consume(Token.FromValue( TokenType.Digit, 3));
            root.Consume(Token.EOF);

            root.IsComplete.Should().BeTrue();
            root.Children.Should().HaveCount(1);
            root.Children.First().Should().BeOfType<MathOperationNode>();
            var mathNode = root.Children.First().As<MathOperationNode>();
            mathNode.LeftOperand.Should().BeOfType<NumberNode>();
            mathNode.RightOperand.Should().BeOfType<NumberNode>();
            mathNode.LeftOperand.Should().NotBeNull();
            mathNode.RightOperand.Should().NotBeNull();
            var leftNode = mathNode.LeftOperand.As<NumberNode>();
            var rightNode = mathNode.RightOperand.As<NumberNode>();
            leftNode.Value.Should().Be(1);
            rightNode.Value.Should().Be(3);
        }

        [Fact]
        public void BasicBrackets()
        {
            //(1+3)
            var root = new RootNode();
            root.Consume(Token.OpenBracket);
            root.Consume(Token.FromValue(TokenType.Digit, 1));
            root.Consume(Token.Plus);
            root.Consume(Token.FromValue(TokenType.Digit, 3));
            root.Consume(Token.CloseBracket);
            root.Consume(Token.EOF);

            root.IsComplete.Should().BeTrue();
            root.Children.Should().HaveCount(1);

            root.Children.First().Should().BeOfType<BracketsNode>();
            var bracketsNode =  root.Children.First().As<BracketsNode>();
            bracketsNode.IsComplete.Should().BeTrue();
            bracketsNode.Children.Should().HaveCount(1);
            bracketsNode.Children.First().Should().BeOfType<MathOperationNode>();

            var mathNode = bracketsNode.Children.First().As<MathOperationNode>();

            mathNode.LeftOperand.Should().BeOfType<NumberNode>();
            mathNode.RightOperand.Should().BeOfType<NumberNode>();

            var leftNode = mathNode.LeftOperand.As<NumberNode>();
            var rightNode = mathNode.RightOperand.As<NumberNode>();
            leftNode.Value.Should().Be(1);
            rightNode.Value.Should().Be(3);
        }

        [Fact]
        public void MultiBrackets()
        {
            var root = new RootNode();
            // (())
            root.Consume(Token.OpenBracket);
            root.Consume(Token.OpenBracket);
            root.Consume(Token.CloseBracket);
            root.Consume(Token.CloseBracket);
            root.Consume(Token.EOF);

            root.IsComplete.Should().BeTrue();
            root.Children.Should().HaveCount(1);

            root.Children.First().Should().BeOfType<BracketsNode>();
            var bracketsNode =  root.Children.First().As<BracketsNode>();
            bracketsNode.IsComplete.Should().BeTrue();
            bracketsNode.Children.Should().HaveCount(1);

            bracketsNode.Children.First().Should().BeOfType<BracketsNode>();
            var bracketsNode2 =  root.Children.First().As<BracketsNode>();
            bracketsNode2.IsComplete.Should().BeTrue();
            bracketsNode2.Children.Should().HaveCount(1);
        }

        [Fact]
        public void MultiBracketsWithOperation()
        {
            var root = new RootNode();
            // (11)-(10-(11))
            root.Consume(Token.OpenBracket);
            root.Consume(Token.FromValue(TokenType.Digit, 11));
            root.Consume(Token.CloseBracket);
            root.Consume(Token.Minus);
            root.Consume(Token.OpenBracket);
            root.Consume(Token.FromValue(TokenType.Digit, 10));
            root.Consume(Token.Minus);
            root.Consume(Token.OpenBracket);
            root.Consume(Token.FromValue(TokenType.Digit, 11));
            root.Consume(Token.CloseBracket);
            root.Consume(Token.CloseBracket);
            root.Consume(Token.EOF);

            root.IsComplete.Should().BeTrue();
            root.Children.Should().HaveCount(1);
        }

        [Fact]
        public void SignMath()
        {
            // -3
            var root = new RootNode();
            root.Consume(Token.Minus);
            root.Consume(Token.FromValue( TokenType.Digit, 3));
            root.Consume(Token.EOF);

            root.IsComplete.Should().BeTrue();
            root.Children.Should().HaveCount(1);
            root.Children.First().Should().BeOfType<SignOperationNode>();
            var signNode = root.Children.First().As<SignOperationNode>();
            signNode.Operand.Should().BeOfType<NumberNode>();
            signNode.Operation.Should().BeOfType<SubtractOperationNode>();
        }
    }
}
