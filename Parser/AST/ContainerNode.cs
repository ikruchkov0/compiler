using System;
using System.Collections.Generic;
using System.Linq;
using Lexer;
using Parser.AST.Exceptions;

namespace Parser.AST
{
    public class ContainerNode : ASTNode, INodesContainer
    {
        protected readonly List<ASTNode> Items = new List<ASTNode>();

        protected ASTNode CurrentNode { get; private set; }

        protected void ReplaceLastNode(ASTNode node)
        {
            if (Items.Count == 0)
            {
                throw new UnexpectedNodeException($"Unable to replace last node with {node} because list is empty", node);
            }

            Items[Items.Count-1] = node;
        }

        public IEnumerable<ASTNode> Children => Items;

        public override bool IsComplete => CurrentNode == null || CurrentNode.IsComplete;

        public override string ToString() 
        {
            var children = string.Join(",", Items.Select(n => n.ToString()));
            return children;
        }

        public override void Consume(Token token)
        {
            if (token.TokenType == TokenType.Space)
            {
                // skip space
                return;
            }

            if (CurrentNode != null && !CurrentNode.IsComplete)
            {
                CurrentNode.Consume(token);
                return;
            }

            if (CurrentNode != null && CurrentNode.IsComplete)
            {
                var nextNodeFactory = CurrentNode.Become(token.TokenType);
                if (nextNodeFactory != null)
                {
                    var newNode = nextNodeFactory(token);
                    CurrentNode = newNode;
                    ReplaceLastNode(CurrentNode);
                    return;
                }
            }

            if (token.TokenType == TokenType.EOF)
            {
                // skip EOF
                return;
            }

            CurrentNode = CreateNewNode(token);
            Items.Add(CurrentNode);
        }

        private ASTNode CreateNewNode(Token token)
        {
            switch (token.TokenType)
            {
                case TokenType.OpenBracket:
                    return new BracketsNode();
                case TokenType.Digit:
                case TokenType.Hex:    
                    return new NumberNode(token);
                case TokenType.Plus:
                case TokenType.Minus:
                    return new SignOperationNode(token);
                default:
                    throw new UnexpectedTokenException(token);
            }
        }

    }
}
