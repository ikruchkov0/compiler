namespace Lexer
{
    public enum TokenType
    {
        None, Unknown, OpenBracket, CloseBracket, Plus, Minus, Mul, Div, Space, Digit, EOL, EOF, Hex, Inf
    }
}
