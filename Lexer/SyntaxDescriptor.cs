using System.Collections;
using System.Collections.Generic;

namespace Lexer
{
    class SyntaxDescriptor : IEnumerable<TokenDescriptor>
    {
        private readonly IList<TokenDescriptor> _descriptors;

        public SyntaxDescriptor()
        {
            _descriptors = new List<TokenDescriptor>();
        }

        public void Add(TokenType tokenType, SyntaxNode syntax)
        {
            _descriptors.Add(new TokenDescriptor(tokenType, syntax));
        }

        #region Implementation of IEnumerable

        public IEnumerator<TokenDescriptor> GetEnumerator()
        {
            return _descriptors.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _descriptors.GetEnumerator();
        }

        #endregion
    }
}