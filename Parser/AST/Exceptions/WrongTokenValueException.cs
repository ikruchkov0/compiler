using System;
using Lexer;

namespace Parser.AST.Exceptions
{
    public class WrongTokenValueException : Exception
    {
        public Token Token { get; }
        public WrongTokenValueException(Token token) : base($"Wrong token {token.TokenType} value str{token.String} int{token.Int} at position {token.Position}")
        {
            Token = token;
        }
    }
}
