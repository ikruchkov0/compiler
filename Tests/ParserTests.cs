using System;
using Xunit;
using FluentAssertions;
using Xunit.Abstractions;
using Lexer;
using Parser;
using System.Threading;

namespace Tests
{

    public class ParserTests
    {
        private readonly ITestOutputHelper _output;

        public ParserTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Theory]
        [InlineData("", 0)]
        [InlineData("1", 1)]
        [InlineData("1+1", 2)]
        [InlineData("()", 0)]
        [InlineData("(10)", 10)]
        [InlineData("+(10)", 10)]
        [InlineData("(+55555)", 55555)]
        [InlineData("(10+11)", 21)]
        [InlineData("( 10 + 11 )", 21)]
        [InlineData("( 10 +   1  )", 11)]
        [InlineData("(255)+(144)", 399)]
        [InlineData("(10+11)+(1+2)", 24)]
        [InlineData("1+(21)", 22)]
        [InlineData("()+()", 0)]
        [InlineData("(1)+(1)", 2)]
        [InlineData("(22)+1", 23)]
        [InlineData("(-55555)", -55555)]
        [InlineData("-()", 0)]
        [InlineData("-(10)", -10)]
        [InlineData("0-(10)", -10)]
        [InlineData("(10-10)", 0)]
        [InlineData("(10-(11))", -1)]
        [InlineData("(100-(10+20))", 70)]
        [InlineData("(100-(10+20))-((50)+(20+(20-10)-10))+5", 5)]
        [InlineData("1*2", 2)]
        [InlineData("(1)*(2)", 2)]
        [InlineData("(8*(2+2)", 32)]
        [InlineData("((2+2)*8", 32)]
        [InlineData("2*8 + 1", 17)]
        [InlineData("1 + 2*8", 17)]
        [InlineData("1 + (2*8)", 17)]
        [InlineData("1 + 5 + 5 + 1 + 1 + 2", 15)]
        [InlineData("1 + 2*8 + 3", 20)]
        [InlineData("8/2 + 1", 5)]
        [InlineData("1 + 8/2", 5)]
        [InlineData("1 + (8/2)", 5)]
        [InlineData("1 + 8/2 + 3", 8)]
        [InlineData("(8/2)/2", 2)]
        [InlineData("(8/2)*0", 0)]
        [InlineData("0xFF", 255)]
        [InlineData("0x000F0 + 0x00F", 255)]
        [InlineData("0xA + 1", 11)]
        [InlineData("0xA - 0xF", -5)]
        [InlineData("0x0A + 1", 11)]
        [InlineData("0x0A - 0x0F", -5)]
        [InlineData("(0x0A) - 0x0F", -5)]
        [InlineData("(0x0A - 0x0F)", -5)]
        [InlineData("(0xF0*0x002)", 480)]
        [InlineData("-0xF0", -240)]
        [InlineData("0xF0/0xA", 24)]
        public void Parser(string input, int result)
        {
            var root = new ASTParser().Parse(input, CancellationToken.None);
            _output.WriteLine($"{input} :: {root}");
            
            var actual = new ASTEvaluator(_output).Eval(root);

            actual.Should().Be(result);
        }
    }
}
