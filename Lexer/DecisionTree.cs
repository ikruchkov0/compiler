using System;
using System.Collections.Generic;

namespace Lexer
{
    class DecisionTree
    {
        private readonly SyntaxDescriptor _tokenMap = new SyntaxDescriptor
        {
            {TokenType.OpenBracket, "("},
            {TokenType.CloseBracket, ")"},
            {TokenType.Plus, "+"},
            {TokenType.Minus, "-"},
            {TokenType.Mul, "*"},
            {TokenType.Div, "/"},
            {TokenType.Space, SyntaxNode.SetOf(" ")},
            {TokenType.Digit, SyntaxNode.SetOf("0123456789")},
            {TokenType.Inf, "inf"},
            {TokenType.Hex, SyntaxNode.KeywordAndSetOf("0x", "0123456789ABCDEF")},
        };

        public DecisionTreeNode Root { get; }
        
        public DecisionTree()
        {
            Root = DecisionTreeNode.CreateRoot();
            foreach (var td in _tokenMap)
            {
                ProcessDescriptor(td);
            }
        }

        private void ProcessDescriptor(TokenDescriptor tokenDescriptor)
        {
            var currentNode = ProcessKeyword(Root, tokenDescriptor);
            ProcessSet(currentNode, tokenDescriptor);
        }

        private static DecisionTreeNode ProcessKeyword(DecisionTreeNode startNode, TokenDescriptor tokenDescriptor)
        {
            if (string.IsNullOrEmpty(tokenDescriptor.Keyword))
            {
                return startNode;
            }

            var currentNode = startNode;
            var wasAdded = false;
            foreach (var c in tokenDescriptor.Keyword)
            {
                if (currentNode.TryGetNode(c, out var node))
                {
                    currentNode = node;
                    continue;
                }

                currentNode = currentNode.AddNode(c, tokenDescriptor.Type, false, string.IsNullOrEmpty(tokenDescriptor.Set));
                wasAdded = true;
            }

            if (!wasAdded)
            {
                throw new ArgumentException(
                    $"Key {tokenDescriptor.Keyword} already occupied by {currentNode.TokenType}",
                    nameof(tokenDescriptor.Type));
            }
            
            return currentNode;
        }
        
        private static void ProcessSet(DecisionTreeNode node, TokenDescriptor tokenDescriptor)
        {
            if (string.IsNullOrEmpty(tokenDescriptor.Set))
            {
                return;
            }

            foreach (var c in tokenDescriptor.Set)
            {
                node.AddNode(c, tokenDescriptor.Type, true, false);
            }
        }
    }
}