using System;

namespace Parser.AST.Exceptions
{
    public class UnexpectedNodeException : Exception
    {
        public ASTNode Node { get; }
    
        public UnexpectedNodeException(string message, ASTNode node) : base(message)
        {
            Node = node;
        }

        public UnexpectedNodeException(ASTNode node) : this($"Unexpected node {node.GetType()}", node)
        {
        }
    }
}
