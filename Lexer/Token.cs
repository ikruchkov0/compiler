using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;

namespace Lexer
{
    public struct Token
    {
        public TokenType TokenType { get; }

        public int Position { get; }

        public int Length { get; }

        public string String { get; }

        public int Int { get; }

        public override string ToString()
        {
            var val = !string.IsNullOrEmpty(String) ? $"[{String}]" : Int > int.MinValue ? $"[{Int}]" : string.Empty;
            return $"{TokenType}{val},{Position},{Length}";
        }

        private Token(int position, int length, TokenType tokenType, string strVal, int intVal)
        {
            Position = position;
            TokenType = tokenType;
            Length = length;
            String = strVal;
            Int = intVal;
        }

        public static Token None => new Token(-1, 0, TokenType.None, string.Empty, int.MinValue);
        public static Token EOF => new Token(-1, 0, TokenType.EOF, string.Empty, int.MinValue);
        public static Token OpenBracket => new Token(-1, 0, TokenType.OpenBracket, string.Empty, int.MinValue);
        public static Token CloseBracket => new Token(-1, 0, TokenType.CloseBracket, string.Empty, int.MinValue);
        public static Token Plus => new Token(-1, 0, TokenType.Plus, string.Empty, int.MinValue);
        public static Token Minus => new Token(-1, 0, TokenType.Minus, string.Empty, int.MinValue);

        public static Token Create(int position, int length, TokenType tokenType, ReadOnlySpan<char> input)
        {
            switch (tokenType)
            {
                case TokenType.OpenBracket:
                case TokenType.CloseBracket:
                case TokenType.Plus:
                case TokenType.Minus:
                case TokenType.Mul:
                case TokenType.Div:
                    return new Token(position, length, tokenType, string.Empty, int.MinValue);
                case TokenType.Space:
                    return new Token(position, length, tokenType, string.Empty, int.MinValue);
                case TokenType.Digit:
                    var intSlice = input.Slice(position, length);
                    var intVal = int.Parse(intSlice, NumberStyles.Integer);
                    return new Token(position, length, tokenType, string.Empty, intVal);
                case TokenType.Hex:
                    var hexSlice = input.Slice(position+2, length-2);
                    var hexVal = int.Parse(hexSlice, NumberStyles.HexNumber);
                    return new Token(position, length, tokenType, string.Empty, hexVal);
                case TokenType.Unknown:
                    var strSlice = input.Slice(position, length);
                    return new Token(position, length, tokenType, new string(strSlice), int.MinValue);
                default:
                    throw new ArgumentException($"Unknown token type {tokenType}", nameof(tokenType));
            }
        }

        public static Token FromValue(TokenType tokenType, string value)
        {
            return new Token(-1, 0, tokenType, value, int.MinValue);
        }

        public static Token FromValue(TokenType tokenType, int value)
        {
            return new Token(-1, 0, tokenType, string.Empty, value);
        }
    }
}
