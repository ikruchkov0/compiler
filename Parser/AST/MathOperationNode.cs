using System;
using Lexer;
using Parser.AST.Exceptions;

namespace Parser.AST
{
    public class MathOperationNode : ArithmeticNode
    {
        private bool _isComplete = false;

        public ASTNode LeftOperand { get; private set; }

        public ASTNode RightOperand { get; private set; }

        public ASTNode Operation { get; }

        public MathOperationNode(ASTNode leftOperandNode, Token token)
        {
            LeftOperand = GetLeftOperand(leftOperandNode);
            Operation = CreateOperationNode(token);
        }

        private MathOperationNode(ASTNode leftOperandNode, ASTNode operationNode)
        {
            LeftOperand = leftOperandNode;
            Operation = operationNode;
        }

        private MathOperationNode(ASTNode leftOperandNode, ASTNode rightOperandNode, ASTNode operationNode) : this(leftOperandNode, operationNode)
        {
            RightOperand = rightOperandNode;
        }

        public override void Consume(Token token)
        {
            if (_isComplete)
            {
                throw new Exception($"Already completed {this}", new UnexpectedTokenException(token));
            }
            if (RightOperand != null && !RightOperand.IsComplete)
            {
                RightOperand.Consume(token);
                if (RightOperand.IsComplete)
                {
                    _isComplete = true;
                }
                return;
            }
            switch (token.TokenType)
            {
                case TokenType.Space:
                    // skip space
                    break;
                case TokenType.OpenBracket:
                    RightOperand = new BracketsNode();
                    break;
                case TokenType.Digit:
                case TokenType.Hex:    
                    RightOperand = new NumberNode(token);
                    _isComplete = true;
                    break;
                default:
                    throw new UnexpectedTokenException(token);
            }
        }

        public override bool IsComplete => _isComplete;

        public override string ToString() 
        {
            return $"Math{{{LeftOperand}{Operation}{RightOperand}}}";
        }

        public override ASTFactory Become(TokenType tokenType)
        {
            switch (tokenType)
            {
                case TokenType.Mul:
                case TokenType.Div:
                    var newOp = CreateOperationNodeFromTokenType(tokenType);
                    var newRightOperand = new MathOperationNode(RightOperand, newOp);
                    return token => new MathOperationNode(LeftOperand, newRightOperand, Operation);
                default:
                    return base.Become(tokenType);
            }
        }

        private ASTNode CreateOperationNode(Token token)
        {
            switch (token.TokenType)
            {
                case TokenType.Plus:
                    return new SumOperationNode();
                case TokenType.Minus:
                    return new SubtractOperationNode();
                case TokenType.Mul:
                    return new MultiplyOperationNode();
                case TokenType.Div:
                    return new DivisionOperationNode();
                default:
                    throw new UnexpectedTokenException(token);
            }
        }

        private ASTNode CreateOperationNodeFromTokenType(TokenType tokenType)
        {
            switch (tokenType)
            {
                case TokenType.Mul:
                    return new MultiplyOperationNode();
                case TokenType.Div:
                    return new DivisionOperationNode();
                default:
                    throw new ArgumentException($"Unsupported token type {tokenType}", nameof(tokenType));
            }
        }

        private ASTNode GetLeftOperand(ASTNode node)
        {
            if (node == null)
            {
                throw new ArgumentNullException(nameof(node));
            }
            switch (node)
            {
                case BracketsNode b:
                    return b;
                case NumberNode n:
                    return n;
                case MathOperationNode m:
                    return m;
                case SignOperationNode s:
                    return s;
                default:
                    throw new UnexpectedNodeException(node);
            }
        }
    }
}
