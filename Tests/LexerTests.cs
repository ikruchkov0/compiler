using Xunit;
using FluentAssertions;
using Xunit.Abstractions;
using Lexer;
using Parser;
using System.Threading;

namespace Tests
{

    public class LexerTests
    {
        private readonly ITestOutputHelper _output;

        public LexerTests(ITestOutputHelper output)
        {
            this._output = output;
        }

        [Fact]
        public void Empty()
        {
            var input = "";

            var actual = new ASTParser().Tokenize(input, CancellationToken.None);

            var expected = new[]
            {
                Token.EOF,
            };

            actual.Should().BeEquivalentTo(expected, 
                opts => opts.Including(x => x.TokenType).Including(x => x.Int).ComparingByMembers<Token>()
            );
        }

        [Fact]
        public void SimpleMath()
        {
            var input = "(10   +    11)";

            var actual = new ASTParser().Tokenize(input, CancellationToken.None);

            var expected = new[]
            {
                Token.OpenBracket,
                Token.FromValue(TokenType.Digit, 10),
                Token.FromValue(TokenType.Space, string.Empty),
                Token.Plus,
                Token.FromValue(TokenType.Space, string.Empty),
                Token.FromValue(TokenType.Digit, 11),
                Token.CloseBracket,
                Token.EOF,
            };

            actual.Should().BeEquivalentTo(expected, 
                opts => opts.Including(x => x.TokenType).Including(x => x.Int).ComparingByMembers<Token>()
            );
        }

        [Fact]
        public void Complex()
        {
            var input = "(10-(11))";

            var actual = new ASTParser().Tokenize(input, CancellationToken.None);

            var expected = new[]
            {
                Token.OpenBracket,
                Token.FromValue(TokenType.Digit, 10),
                Token.Minus,
                Token.OpenBracket,
                Token.FromValue(TokenType.Digit, 11),
                Token.CloseBracket,
                Token.CloseBracket,
                Token.EOF,
            };

            actual.Should().BeEquivalentTo(expected, 
                opts => opts.Including(x => x.TokenType).Including(x => x.Int).ComparingByMembers<Token>()
            );
        }
        
        [Fact]
        public void SimpleHex()
        {
            var input = "0xFA";

            var actual = new ASTParser().Tokenize(input, CancellationToken.None);

            var expected = new[]
            {
                Token.FromValue(TokenType.Hex, 250),
                Token.EOF,
            };

            actual.Should().BeEquivalentTo(expected, 
                opts => opts.Including(x => x.TokenType).Including(x => x.Int).ComparingByMembers<Token>()
            );
        }
        
        [Fact]
        public void SimpleWithZeroesHex()
        {
            var input = "0x0F";

            var actual = new ASTParser().Tokenize(input, CancellationToken.None);

            var expected = new[]
            {
                Token.FromValue(TokenType.Hex, 15),
                Token.EOF,
            };

            actual.Should().BeEquivalentTo(expected, 
                opts => opts.Including(x => x.TokenType).Including(x => x.Int).ComparingByMembers<Token>()
            );
        }
        
        [Fact]
        public void HexMixWIthDigit()
        {
            var input = "0xFF + 10";

            var actual = new ASTParser().Tokenize(input, CancellationToken.None);

            var expected = new[]
            {
                Token.FromValue(TokenType.Hex, 255),
                Token.FromValue(TokenType.Space, string.Empty),
                Token.Plus,
                Token.FromValue(TokenType.Space, string.Empty),
                Token.FromValue(TokenType.Digit, 10),
                Token.EOF,
            };

            actual.Should().BeEquivalentTo(expected, 
                opts => opts.Including(x => x.TokenType).Including(x => x.Int).ComparingByMembers<Token>()
            );
        }
    }
}
