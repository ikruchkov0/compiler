using System;
using System.Collections.Generic;
using System.Linq;

namespace Lexer
{

    class DecisionTreeNode
    {
        private readonly Dictionary<char, DecisionTreeNode> _nodes;
        
        public bool HasChildren { get; private set; }
        
        public DecisionTreeNode Parent { get; }

        public TokenType TokenType { get; }
        
        public char Key { get; }
        
        public bool IsRepetable { get; }
        
        public bool IsFinite { get; }

        private DecisionTreeNode(char key, TokenType tokenType, bool isRepetable, bool isFinite, DecisionTreeNode parent)
        {
            _nodes = new Dictionary<char, DecisionTreeNode>();
            TokenType = tokenType;
            Key = key;
            Parent = parent;
            IsRepetable = isRepetable;
            IsFinite = isFinite;
        }

        public DecisionTreeNode AddNode(char c, TokenType tokenType, bool isRepetable, bool isFinite)
        {
            var node = new DecisionTreeNode(c, tokenType, isRepetable, isFinite, this);
            _nodes.Add(c, node);
            HasChildren = true;
            return node;
        }
        
        public bool TryGetNode(char c, out DecisionTreeNode node)
        {
            return _nodes.TryGetValue(c, out node);
        }
        
        public override string ToString()
        {
            var nodesStr = string.Empty;
            if (HasChildren)
            {
                var nodes = _nodes.Values.Select(n => n.ToString());
                nodesStr = $"->{string.Join(",", nodes)}";
            }
            
            return $"[{Key}]{TokenType}{nodesStr}";
        }
        
        public static DecisionTreeNode CreateRoot() => new DecisionTreeNode(char.MinValue, TokenType.Unknown, true, false, null);
    }
}