using System;
using Xunit.Abstractions;
using Parser.AST;

namespace Tests
{
    class ASTEvaluator
    {
        private readonly ITestOutputHelper _output;

        public ASTEvaluator(ITestOutputHelper output)
        {
            this._output = output;
        }

        public int Eval(ASTNode node)
        {
            switch (node)
            {
                case INodesContainer c:
                    return Eval(c);
                case MathOperationNode m:
                    return Eval(m);
                case SignOperationNode s:
                    return Eval(s);
                case NumberNode n:
                    return n.Value;
                default:
                    throw new Exception($"Unknown node {node}");
            }
        }

        private int Eval(INodesContainer c)
        {
            var result = 0;
            foreach (var node in c.Children)
            {
                result += Eval(node);
            }
            return result;
        }

        private int Eval(MathOperationNode node)
        {
            var a = Eval(node.LeftOperand);
            var b = Eval(node.RightOperand);
            var op = EvalOperation(node.Operation);
            return op(a, b);
        }

        private int Eval(SignOperationNode node)
        {
            var a = 0;
            var b = Eval(node.Operand);
            var op = EvalOperation(node.Operation);
            return op(a, b);
        }

        private Func<int, int, int> EvalOperation(ASTNode node)
        {
            switch (node)
            {
                case SumOperationNode n:
                    return (a, b) => a + b;
                case SubtractOperationNode n:
                    return (a, b) => a - b;
                case MultiplyOperationNode n:
                    return (a, b) => a * b;
                case DivisionOperationNode n:
                    return (a, b) => a / b;
                default:
                    throw new Exception($"Unknown node {node}");
            }
        }
    }
}
