using System;
using Lexer;

namespace Parser.AST
{
    public class SumOperationNode : ASTNode
    {
        public override void Consume(Token token)
        {
            throw new InvalidOperationException("Not supported");
        }

        public override bool IsComplete => true;

        public override string ToString() 
        {
            return "+";
        }
    }
}
