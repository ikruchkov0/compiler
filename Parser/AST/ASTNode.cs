using Lexer;

namespace Parser.AST
{
    public delegate ASTNode ASTFactory(Token token);

    public abstract class ASTNode
    {
        
        public abstract bool IsComplete { get; }
        public abstract void Consume(Token token);
        public abstract override string ToString();
        public virtual ASTFactory Become(TokenType tokenType)
        {
            return null;
        }
    }
}
