using System.Linq;

namespace Parser.AST
{
    public class RootNode : ContainerNode
    {
        public override string ToString() 
        {
            return $"Root({base.ToString()})";
        }
    }
}
