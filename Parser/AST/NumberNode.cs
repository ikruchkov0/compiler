using System;
using Lexer;
using Parser.AST.Exceptions;

namespace Parser.AST
{
    public class NumberNode : ArithmeticNode
    {
        public int Value { get; }


        public NumberNode(Token token)
        {
            switch (token.TokenType)
            {
                case TokenType.Digit:
                case TokenType.Hex:
                    Value = token.Int;
                    break;
                default:
                    throw new UnexpectedTokenException(token);
            }
        }

        public override bool IsComplete => true;

        public override void Consume(Token token)
        {
            throw new InvalidOperationException("Not supported");
        }

        public override string ToString()
        {
            return $"{Value}";
        }
    }
}
