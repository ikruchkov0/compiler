using Lexer;

namespace Parser.AST
{
    public abstract class ArithmeticNode : ASTNode
    {
        public override ASTFactory Become(TokenType tokenType)
        {
            return BecomeOperand(tokenType);
        }

        private ASTFactory BecomeOperand(TokenType tokenType)
        {
            switch (tokenType)
            {
                case TokenType.Minus:
                case TokenType.Plus:
                case TokenType.Mul:
                case TokenType.Div:
                    return token => new MathOperationNode(this, token);
                default:
                    return null;
            }
        }
    }
}