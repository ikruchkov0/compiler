using System;
using Lexer;
using Parser.AST.Exceptions;

namespace Parser.AST
{
    public class SignOperationNode : ArithmeticNode
    {
        private bool _isComplete = false;

        public ASTNode Operand { get; private set; }

        public ASTNode Operation { get; }

        public SignOperationNode(Token token)
        {
            Operation = CreateOperationNode(token);
        }

        public override void Consume(Token token)
        {
            if (_isComplete)
            {
                throw new Exception($"Already completed {this}", new UnexpectedTokenException(token));
            }
            if (Operand != null && !Operand.IsComplete)
            {
                Operand.Consume(token);
                if (Operand.IsComplete)
                {
                    _isComplete = true;
                }
                return;
            }
            switch (token.TokenType)
            {
                case TokenType.Space:
                    // skip space
                    break;
                case TokenType.OpenBracket:
                    Operand = new BracketsNode();
                    break;
                case TokenType.Digit:
                case TokenType.Hex:    
                    Operand = new NumberNode(token);
                    _isComplete = true;
                    break;
                default:
                    throw new UnexpectedTokenException(token);
            }
        }

        public override bool IsComplete => _isComplete;

        public override string ToString() 
        {
            return $"Sign{{{Operation}{Operand}}}";
        }

        private ASTNode CreateOperationNode(Token token)
        {
            switch (token.TokenType)
            {
                case TokenType.Plus:
                    return new SumOperationNode();
                case TokenType.Minus:
                    return new SubtractOperationNode();
                default:
                    throw new UnexpectedTokenException(token);
            }
        }
    }
}
