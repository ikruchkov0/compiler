namespace Lexer
{
    class SyntaxNode
    {
        public string Keyword { get; private set; }
        
        public string Set { get; private set; }

        private SyntaxNode()
        {
        }
        
        public static SyntaxNode SetOf(string str)
        {
            return new SyntaxNode { Set = str};
        }
        
        public static SyntaxNode KeywordAndSetOf(string startWith, string continueWithSet)
        {
            return new SyntaxNode { Keyword = startWith, Set = continueWithSet };
        }

        public static implicit operator SyntaxNode(string str)
        {
            return new SyntaxNode { Keyword = str };
        }
    }
}