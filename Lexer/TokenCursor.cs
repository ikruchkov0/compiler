
using System;

namespace Lexer
{
    public class TokenCursor
    {
        private TokenType _lastTokenType = TokenType.None;
        private int _pos = 0;
        private int _tokenStart = 0;
        private int _tokenLength = 0;

        private readonly DecisionTree _decisionTree;
        
        private readonly Action<string> _logger;
        
        private DecisionTreeNode _decisionNode;

        public TokenCursor()
        {
            _decisionTree = new DecisionTree();
            _decisionNode = _decisionTree.Root;
        }

        public bool TryReadNextToken(ReadOnlySpan<char> source, out Token token)
        {
            var result = false;
            Token resultToken = Token.None;
            var tokenType = _pos < source.Length ? SwitchTokenType(source[_pos]) : TokenType.EOF;
            if (ShouldProduceToken(tokenType))
            {
                resultToken = ReadLastToken(source);
                _tokenLength = 0;
                _tokenStart = _pos;
                result = true;
            }
            _tokenLength++;
            _lastTokenType = tokenType;
            _pos++;
            token = resultToken;
            
            return result;
        }

        private Token ReadLastToken(ReadOnlySpan<char> input)
        {
            if (_lastTokenType == TokenType.EOF)
            {
                return Token.EOF;
            }
            return Token.Create(_tokenStart, _tokenLength, _lastTokenType, input);
        }

        private bool ShouldProduceToken(TokenType tokenType)
        {
            if (_lastTokenType == TokenType.None)
            {
                return false;
            }

            if (tokenType == TokenType.EOF)
            {
                return true;
            }

            if (_tokenLength == 0)
            {
                return false;
            }

            if (_lastTokenType != tokenType)
            {
                return true;
            }

            if (_decisionNode.IsFinite)
            {
                return true;
            }

            return false;
        }

        private TokenType SwitchTokenType(char c)
        {
            _decisionNode = GetNode(c);
            return _decisionNode.TokenType;
        }
        
        private DecisionTreeNode GetNode(char c)
        {
            if (_decisionNode.Key == c && _decisionNode.IsRepetable) // same node
            {
                return _decisionNode;
            }

            if (_decisionNode.HasChildren && _decisionNode.TryGetNode(c, out var child)) // try get from current
            {
                _lastTokenType = child.TokenType;
                return child;
            }

            if (!_decisionNode.HasChildren && _decisionNode.IsRepetable &&
                _decisionNode.Parent.TryGetNode(c, out var parentChild))
            {
                return parentChild;
            }
            
            if (_decisionTree.Root.TryGetNode(c, out var rootChild)) // try get from root
            {
                return rootChild;
            }
            return _decisionTree.Root;
        }
        
    }
}
