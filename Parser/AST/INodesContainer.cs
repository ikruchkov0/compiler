using System.Collections.Generic;

namespace Parser.AST
{
    public interface INodesContainer
    {
        IEnumerable<ASTNode> Children { get; }
    }
}
