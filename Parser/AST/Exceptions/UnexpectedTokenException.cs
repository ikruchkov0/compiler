using System;
using Lexer;

namespace Parser.AST.Exceptions
{
    public class UnexpectedTokenException : Exception
    {
        public Token Token { get; }
        public UnexpectedTokenException(string msg, Token token) : base(msg)
        {
            Token = token;
        }

        public UnexpectedTokenException(Token token) : this($"Unexpected token {token.TokenType} at position {token.Position}", token)
        {
        }
    }
}
