using System;
using System.Collections.Generic;
using System.Threading;
using Lexer;
using Parser.AST;

namespace Parser
{
    public class ASTParser
    {

        public RootNode Parse(ReadOnlySpan<char> input, CancellationToken cancellationToken)
        {
            var root = new RootNode();
            Read(input, t => root.Consume(t), cancellationToken);
            return root;
        }

        public IEnumerable<Token> Tokenize(ReadOnlySpan<char> input, CancellationToken cancellationToken)
        {
            var list = new List<Token>();
            Read(input, t => list.Add(t), cancellationToken);
            return list;
        }

        private void Read(ReadOnlySpan<char> input, Action<Token> consume, CancellationToken cancellationToken)
        {
            TokenCursor cursor = new TokenCursor();
            Token token = Token.None;
            while (token.TokenType != TokenType.EOF)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    break;
                }
                if (cursor.TryReadNextToken(input, out token))
                {
                    consume(token);
                }
            }
        }

    }
}
